### 版本说明
本示例配套的SDK版本为：3.1.2及以上版本

### 示例简介
本示例展示如何使用企业路由器实例（EnterpriseRouter）相关SDK

### 功能介绍
该代码示例展示了企业路由器实例的增、删、改、查功能。由于创建、更新和删除操作是异步接口，在进行调试时，需要注释无关的代码。
以创建企业路由器实例为例, 进行测试时，将其它功能的测试函数注释掉。
```java
    public static void main(String[] args) {
        String ak = "<your ak>";
        String sk = "<your sk>";
        String erId = "{er_id}";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        ErClient client = ErClient.newBuilder()
            .withCredential(auth)
            .withRegion(ErRegion.valueOf("cn-south-1"))
            .build();

        // Create enterprise router
        createEnterpriseRouter(client);

        /*
         * the parameter erId can be extracted from response of the previous function
         */
        // Update enterprise router
        // updateEnterpriseRouter(client, erId);

        // Show enterprise router
        // showEnterpriseRouter(client, erId);

        // Delete enterprise router
        // deleteEnterpriseRouter(client, erId);

        // List enterprise routers
        // listEnterpriseRouters(client);

    }
```

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

### SDK获取和安装
您可以通过Maven配置所依赖的企业路由器服务SDK

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java)  (产品类别：企业路由器)
```xml
<dependency>
    <groupId>com.huaweicloud.sdk</groupId>
    <artifactId>huaweicloud-sdk-er</artifactId>
    <version>3.1.2</version>
</dependency>
```
### 代码示例
以下代码展示如何使用企业路由器实例（EnterpriseRouter）相关SDK
``` java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.er.v3.ErClient;
import com.huaweicloud.sdk.er.v3.model.CreateEnterpriseRouterRequest;
import com.huaweicloud.sdk.er.v3.model.CreateEnterpriseRouterRequestBody;
import com.huaweicloud.sdk.er.v3.model.CreateEnterpriseRouterResponse;
import com.huaweicloud.sdk.er.v3.model.DeleteEnterpriseRouterRequest;
import com.huaweicloud.sdk.er.v3.model.DeleteEnterpriseRouterResponse;
import com.huaweicloud.sdk.er.v3.model.ListEnterpriseRoutersRequest;
import com.huaweicloud.sdk.er.v3.model.ListEnterpriseRoutersResponse;
import com.huaweicloud.sdk.er.v3.model.ShowEnterpriseRouterRequest;
import com.huaweicloud.sdk.er.v3.model.ShowEnterpriseRouterResponse;
import com.huaweicloud.sdk.er.v3.model.UpdateEnterpriseRouterRequest;
import com.huaweicloud.sdk.er.v3.model.UpdateEnterpriseRouterRequestBody;
import com.huaweicloud.sdk.er.v3.model.UpdateEnterpriseRouterResponse;
import com.huaweicloud.sdk.er.v3.region.ErRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class EnterpriseRouterInstanceDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseRouterInstanceDemo.class.getName());

    public static void main(String[] args) {
        String ak = "<your ak>";
        String sk = "<your sk>";
        String erId = "{er_id}";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        ErClient client = ErClient.newBuilder()
            .withCredential(auth)
            .withRegion(ErRegion.valueOf("cn-south-1"))
            .build();

        // Create enterprise router
        createEnterpriseRouter(client);

        /*
         * the parameter erId can be extracted from response of the previous function
         */
        // Update enterprise router
        updateEnterpriseRouter(client, erId);

        // Show enterprise router
        showEnterpriseRouter(client, erId);

        // Delete enterprise router
        deleteEnterpriseRouter(client, erId);

        // List enterprise routers
        listEnterpriseRouters(client);

    }

    private static CreateEnterpriseRouterResponse createEnterpriseRouter(ErClient client) {
        CreateEnterpriseRouterRequest request = new CreateEnterpriseRouterRequest();
        CreateEnterpriseRouterRequestBody body = new CreateEnterpriseRouterRequestBody();

        body.withInstance(instance -> {
            instance.withName("<your enterprise router name>");
            instance.withDescription("<your enterprise router description>");
            instance.withAvailabilityZoneIds(azs -> azs.add("<availablility zone id>"));
            instance.withEnableDefaultAssociation(false);
            instance.withEnableDefaultPropagation(false);
            instance.withAsn(64512L); // AS number
        });
        request.withBody(body);

        Function<Void, CreateEnterpriseRouterResponse> task = (Void v) -> {
            return client.createEnterpriseRouter(request);
        };
        return execute(task);
    }

    private static ShowEnterpriseRouterResponse showEnterpriseRouter(ErClient client, String erId) {
        ShowEnterpriseRouterRequest request = new ShowEnterpriseRouterRequest().withErId(erId);
        Function<Void, ShowEnterpriseRouterResponse> task = (Void v) -> {
            return client.showEnterpriseRouter(request);
        };
        return execute(task);
    }

    private static UpdateEnterpriseRouterResponse updateEnterpriseRouter(ErClient client, String erId) {
        UpdateEnterpriseRouterRequest request = new UpdateEnterpriseRouterRequest();
        UpdateEnterpriseRouterRequestBody body = new UpdateEnterpriseRouterRequestBody();
        body.withInstance(instance -> {
            instance.withName("<your new enterprise router name>");
            instance.withDescription("<your new enterprise router description>");
        });
        request.withErId(erId).withBody(body);
        Function<Void, UpdateEnterpriseRouterResponse> task = (Void v) -> {
            return client.updateEnterpriseRouter(request);
        };
        return execute(task);
    }

    private static void deleteEnterpriseRouter(ErClient client, String erId) {
        DeleteEnterpriseRouterRequest request = new DeleteEnterpriseRouterRequest().withErId(erId);
        Function<Void, DeleteEnterpriseRouterResponse> task = (Void v) -> {
            return client.deleteEnterpriseRouter(request);
        };
        execute(task);
    }

    private static ListEnterpriseRoutersResponse listEnterpriseRouters(ErClient client) {
        ListEnterpriseRoutersRequest request = new ListEnterpriseRoutersRequest();
        Function<Void, ListEnterpriseRoutersResponse> task = (Void v) -> {
            return client.listEnterpriseRouters(request);
        };
        return execute(task);
    }

    private static <T> T execute(Function<Void, T> task) {
        T response = null;
        try {
            response = task.apply(null);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
        return response;
    }

}
```
您可以在 [企业路由器服务文档](https://support.huaweicloud.com/productdesc-er/er_01_0002.html) 和[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=ER&api=CreateEnterpriseRouter) 查看具体信息。

### 修订记录

发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
2022/10/10 |1.0 | 文档首次发布
